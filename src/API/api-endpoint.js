import axios from 'axios'
axios.defaults.baseURL = 'https://bust-node-server.herokuapp.com'
axios.defaults.headers.post['Content-Type'] = 'application/json'
var USER = '/user'
var FILE = '/file'

const setToJsonHeader = () => {
  axios.defaults.headers.post['Content-Type'] = 'application/json'
}
const setToMultiPart = () => {
  axios.defaults.headers.post['Content-Type'] = 'application/json'
}

export default {
  async login (credentials) {
    setToJsonHeader()
    var user = await axios.post(`${USER}/login`, credentials)
    return user
  },

  async register (data) {
    setToJsonHeader()
    var status = await axios.post(`${USER}/registration`, data)
    return status
  },

  async hashpassword (data) {
    setToJsonHeader()
    var status = await axios.post(`${USER}/make-password`, data)
    return status
  },

  async uploadImage (data) {
    setToMultiPart()
    var status = await axios.post(`${FILE}/image-upload`, data)
    return status
  }
}
