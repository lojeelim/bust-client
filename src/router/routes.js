
const routes = [
  {
    // Landing Page Routes
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      /* reason: remove for user experience purposes becuase when the server started, it will auto redirecting to  login page
        and when user clicking the link to other page it will redicted to login page firts
      */
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/register', component: () => import('pages/Register.vue') },
      { path: '/signup', component: () => import('../mobile/MobilePages/Signup.vue') }
    ]
  },

  {
    // Dashboard Page Routes
    path: '/dashboard',
    component: () => import('layouts/DashboardLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Dashboard.vue') },
      { path: '/barangay-member', component: () => import('pages/BarangayMember/BarangayMember.vue') },
      { path: '/stations', component: () => import('pages/Station/Station.vue') },
      { path: '/complaint', component: () => import('pages/Complaint/Complaint.vue') },
      { path: '/police', component: () => import('pages/Police/Police.vue') },
      { path: '/department', component: () => import('pages/Department/Department.vue') },
      { path: '/incident', component: () => import('pages/Incident/Incident.vue') },
      { path: '/station', component: () => import('pages/Station/Station.vue') },
      { path: '/citizen-arrest', component: () => import('pages/CitizenArrest/citizenArrest.vue') },
      { path: '/barangay', component: () => import('pages/Barangay/barangay.vue') },
      { path: '/profile', component: () => import('pages/Profile/profile.vue') },
      { path: '/buster', component: () => import('pages/Buster/buster.vue') },
      { path: '/master-list', component: () => import('pages/Blotter/blotterMasterList.vue') }
      // { path: '/file-blotter', component: () => import('pages/Complaint/confirmBlotter.vue') }
    ]
  },

  {
    // Mobile Home page Routes
    path: '/home',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '/', component: () => import('src/mobile/MobilePages/Mobile_Home.vue') },
      { path: '/report', component: () => import('src/mobile/MobilePages/Mobile_Report.vue') },
      { path: '/blotter', component: () => import('src/mobile/MobilePages/Mobile_Blotter.vue') },
      { path: '/account', component: () => import('src/mobile/MobilePages/Mobile_Account.vue') },
      { path: '/task', component: () => import('src/mobile/MobilePages/Mobile_Task.vue') },
      { path: '/notification', component: () => import('src/mobile/MobilePages/Mobile_Notification.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
