const PoliceRank = [
  'Patrolman/Patrolwoman, Pat',
  'Police Corporal, PCpl',
  'Police Staff Sergeant, PSSg',
  'Police Master Sergeant, PMSg',
  'Police Senior Master Sergeant, PSMS',
  'Police Chief Master Sergeant, PCMS',
  'Police Executive Master Sergeant, PEMS',
  'Police Lieutenant, PLT',
  'Police Captain, PCPT',
  'Police Major, PMAJ'
]
export default PoliceRank
