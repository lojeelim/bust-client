const PoliceRank = [
  'Officer In-charge',
  'Assistant Station Chief',
  'Chief Clerk',
  'Admin PNCO',
  'Intel PNCO',
  'Operation PNCO',
  'Suppy PNCO',
  'PCAD PNCO',
  'FINANCE PNCO',
  'INVESTIGATOR',
  'WCPD PNCO',
  'PSMUPNCO'
]
export default PoliceRank
