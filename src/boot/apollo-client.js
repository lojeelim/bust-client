import Vue from 'vue'
import ApolloClient from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'
import gql from 'graphql-tag'

export default async () => {
  Vue.prototype.$apollo = new ApolloClient({
    uri: 'https://bust-hasura.herokuapp.com/v1/graphql',
    cache: new InMemoryCache(),
    headers: {
      'x-hasura-admin-secret': 'bustapplication'
    }
  })
  Vue.prototype.$gql = gql
}
