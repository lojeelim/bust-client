import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = 'https://bust-node-server.herokuapp.com'
axios.defaults.headers.post['Content-Type'] = 'application/json'

Vue.prototype.$axios = axios
