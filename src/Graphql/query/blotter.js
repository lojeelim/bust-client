import gql from 'graphql-tag'

export const createBlotter = gql`mutation createBlotter(
    $status: String,
    $from: date
    $to: date
    $complaint_id: uuid,
    $prepared_by_id: uuid,
    $station_id: uuid,
    $user_id: uuid
  )
  {
    insert_blotter(objects: {
        status:  $status,
        from: $from
        to: $to
        complaint_id: $complaint_id,
        prepared_by_id: $prepared_by_id,
        station_id: $station_id,
        user_id: $user_id
    }) {
        returning {
          id
        }
      }
}
`
export const getAskBlotter = gql`
query MyQuery($id: uuid) {
  blotter(where: {id: {_eq: $id}}) {
    id
    ItemDs {
      blotter_entry_number
      blotter_id
      chief_instructions
      date
      id
      desk_offiicer
      name_of_chief
      name_of_investigator
      narrative_of_incident
      place_of_incident
      reporting_person
      time
      type_of_incident
    }
  }
}
`
export const showBlotterByUserId = gql`
query showBlotterByUserId($user_id: uuid) {
  blotter(where: {user_id: {_eq: $user_id}, _and: {_not: {}, status: {_eq: "For Filling"}}}) {
    status
    created_at
    id
    from
    to
    userByPreparedById {
      first_name
      contact_number
      id
      last_name
      middle_name
      rank
      username
      user_type
    }
    station {
      id
      station_location
      station_logo
      station_name
      station_type
    }
    user {
      first_name
      id
      last_name
      middle_name
      username
    }
    complaint {
      id
      latitude
      longitude
      created_at
      id
      latitude
      longitude
      created_at
      complaint_type
      complaint_evid
      complaint_statement
      complaint_accuse_person
      respondent_activities {
        user {
          last_name
          middle_name
          rank
          first_name
          image
          contact_number
          username
        }
      }
    }
  }
}
`
export const updateBlotterStatus = gql`
mutation updateStatus($id: uuid, $status: String) {
  update_blotter(where: {id: {_eq: $id}}, _set: {status: $status}) {
    affected_rows
  }
}
`

export const GetUserBlotter = gql`
query showBlotterByUserId($user_id: uuid, $status: String) {
  blotter(where: {user_id: {_eq: $user_id}, status: {_eq: $status}}) {
    status
    created_at
    updated_at
    id
    from
    to
    userByPreparedById {
      first_name
      contact_number
      id
      last_name
      middle_name
      rank
      username
      user_type
    }
    station {
      id
      station_location
      station_logo
      station_name
      station_type
    }
    user {
      first_name
      id
      last_name
      middle_name
      username
    }
    complaint {
      id
      from
      to
      latitude
      longitude
      created_at
      complaint_type
      complaint_evid
      complaint_statement
      complaint_accuse_person
    }
    ItemAs {
      id
      age
      blotter_entry_number
      blotter_id
      citizen
      current_home
      current_barangay
      current_province
      current_sitio
      current_town
      date_of_birth
      date_time_incident
      date_time_reported
      email
      family_name
      highest_education
      gender
      id_presented
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      type_of_incident
      created_at
      updated_at
    }
    ItemBs {
      blotter_id
      citizen
      color_hair
      color_of_eye
      created_at
      current_barangay
      current_home
      current_sitio
      current_province
      current_town
      date_of_birth
      desc_eye
      desc_of_hair
      email
      family_name
      gender
      group_affiliation
      height
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      personel_rank
      phone
      place_of_birth
      previous_criminal_record
      qualifier
      relation_to_victim
      status
      status_of_previous_case
      under_influence_of
      unit_assignment
      updated_at
      weight
      work_address
      age
    }
    ItemCs {
      age
      blotter_id
      citizen
      created_at
      current_barangay
      current_home
      current_province
      current_sitio
      current_town
      date_of_birth
      email
      family_name
      gender
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      updated_at
      work_address
    }
    ItemDs {
      blotter_id
      blotter_entry_number
      chief_instructions
      created_at
      date
      desk_offiicer
      id
      name_of_chief
      name_of_investigator
      narrative_of_incident
      place_of_incident
      reporting_person
      time
      type_of_incident
      updated_at
    }
    children_in_conflicts {
      blotter_id
      created_at
      deversion
      distinguising
      guardian_address
      id
      mobile
      name_of_guardian
      phone
      updated_at
    }
    IRTRs {
      blotter_id
      chief_mobile
      chief
      investigator
      investigator_mobile
      station_name
      station_telephone
    }
    Incident_record_reciepts {
      blotter_entry
      address_reporting
      blotter_id
      created_at
      date_incident
      date_report
      desk_officer
      id
      name_of_reporting
      place_incident
      type_incident
      updated_at
    }
  }
}
`
export const Blotter = gql`
query getBlotterByStationId($station_id: uuid) {
  blotter(where: {station_id: {_eq: $station_id}, _and: {status: {_eq: "Blotter"}}}) {
    status
    created_at
    updated_at
    id
    from
    to
    userByPreparedById {
      first_name
      contact_number
      id
      last_name
      middle_name
      rank
      username
      user_type
    }
    station {
      id
      station_location
      station_logo
      station_name
      station_type
    }
    user {
      first_name
      id
      last_name
      middle_name
      username
      image
    }
    complaint {
      id
      from
      to
      latitude
      longitude
      created_at
      complaint_type
      complaint_evid
      complaint_statement
      complaint_accuse_person
      respondent_activities {
        user {
          id
          last_name
          middle_name
          rank
          first_name
          image
          contact_number
          username
        }
        complaint_id
        id
      }
    }
    ItemAs {
      id
      age
      blotter_entry_number
      blotter_id
      citizen
      current_home
      current_barangay
      current_province
      current_sitio
      current_town
      date_of_birth
      date_time_incident
      date_time_reported
      email
      family_name
      highest_education
      gender
      id_presented
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      type_of_incident
      created_at
      updated_at
    }
    ItemBs {
      blotter_id
      citizen
      color_hair
      color_of_eye
      created_at
      current_barangay
      current_home
      current_sitio
      current_province
      current_town
      date_of_birth
      desc_eye
      desc_of_hair
      email
      family_name
      gender
      group_affiliation
      height
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      personel_rank
      phone
      place_of_birth
      previous_criminal_record
      qualifier
      relation_to_victim
      status
      status_of_previous_case
      under_influence_of
      unit_assignment
      updated_at
      weight
      work_address
      age
    }
    ItemCs {
      age
      blotter_id
      citizen
      created_at
      current_barangay
      current_home
      current_province
      current_sitio
      current_town
      date_of_birth
      email
      family_name
      gender
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      updated_at
      work_address
    }
    ItemDs {
      blotter_id
      blotter_entry_number
      chief_instructions
      created_at
      date
      desk_offiicer
      id
      name_of_chief
      name_of_investigator
      narrative_of_incident
      place_of_incident
      reporting_person
      time
      type_of_incident
      updated_at
    }
    children_in_conflicts {
      blotter_id
      created_at
      deversion
      distinguising
      guardian_address
      id
      mobile
      name_of_guardian
      phone
      updated_at
    }
    Incident_record_reciepts {
      blotter_entry
      address_reporting
      blotter_id
      created_at
      date_incident
      date_report
      desk_officer
      id
      name_of_reporting
      place_incident
      type_incident
      updated_at
    }
  }
}
`

export const GetBlotter = gql`
query showBlotterByStationId($station_id: uuid, $status: String) {
  blotter(where: {station_id: {_eq: $station_id}, status: {_eq: $status}}) {
    status
    created_at
    updated_at
    id
    from
    to
    userByPreparedById {
      first_name
      contact_number
      id
      last_name
      middle_name
      rank
      username
      user_type
    }
    station {
      id
      station_location
      station_logo
      station_name
      station_type
    }
    user {
      first_name
      id
      last_name
      middle_name
      username
      image
    }
    complaint {
      id
      from
      to
      latitude
      longitude
      created_at
      complaint_type
      complaint_evid
      complaint_statement
      complaint_accuse_person
      respondent_activities {
        user {
          last_name
          middle_name
          rank
          first_name
          image
          contact_number
          username
        }
      }
    }
    ItemAs {
      id
      age
      blotter_entry_number
      blotter_id
      citizen
      current_home
      current_barangay
      current_province
      current_sitio
      current_town
      date_of_birth
      date_time_incident
      date_time_reported
      email
      family_name
      highest_education
      gender
      id_presented
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      type_of_incident
      created_at
      updated_at
    }
    ItemBs {
      blotter_id
      citizen
      color_hair
      color_of_eye
      created_at
      current_barangay
      current_home
      current_sitio
      current_province
      current_town
      date_of_birth
      desc_eye
      desc_of_hair
      email
      family_name
      gender
      group_affiliation
      height
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      personel_rank
      phone
      place_of_birth
      previous_criminal_record
      qualifier
      relation_to_victim
      status
      status_of_previous_case
      under_influence_of
      unit_assignment
      updated_at
      weight
      work_address
      age
    }
    ItemCs {
      age
      blotter_id
      citizen
      created_at
      current_barangay
      current_home
      current_province
      current_sitio
      current_town
      date_of_birth
      email
      family_name
      gender
      highest_education
      id
      last_name
      middle_name
      mobile
      nickname
      occupation
      other_barangay
      other_home
      other_province
      other_sitio
      other_town
      phone
      place_of_birth
      qualifier
      status
      updated_at
      work_address
    }
    ItemDs {
       blotter_id
      blotter_entry_number
      chief_instructions
      created_at
      date
      desk_offiicer
      id
      name_of_chief
      name_of_investigator
      narrative_of_incident
      place_of_incident
      reporting_person
      time
      type_of_incident
      updated_at
    }
    children_in_conflicts {
      blotter_id
      created_at
      deversion
      distinguising
      guardian_address
      id
      mobile
      name_of_guardian
      phone
      updated_at
    }
    Incident_record_reciepts {
      blotter_entry
      address_reporting
      blotter_id
      created_at
      date_incident
      date_report
      desk_officer
      id
      name_of_reporting
      place_incident
      type_incident
      updated_at
    }
  }
}
`
export const saveReporter = gql`
mutation MyMutation($Aage: String, $Ablotter_entry_number: String, $Acitizen: String, $Acurrent_barangay: String, $Acurrent_home: String, $Acurrent_province: String, $Acurrent_sitio: String, $Acurrent_town: String, $Adate_of_birth: String, $Adate_time_incident: String, $Adate_time_reported: String, $Aemail: String, $Afamily_name: String, $Agender: String, $Ahighest_education: String, $Aid_presented: String, $Alast_name: String, $Amiddle_name: String, $Amobile: String, $Anickname: String, $Aoccupation: String, $Aother_barangay: String, $Aother_home: String, $Aother_province: String, $Aother_sitio: String, $Aother_town: String, $Aphone: String, $Aplace_of_birth: String, $Aqualifier: String, $Astatus: String, $Atype_of_incident: String, $Ablotter_id: uuid) {
  insert_ItemA(objects: {age: $Aage, blotter_entry_number: $Ablotter_entry_number, citizen: $Acitizen, current_barangay: $Acurrent_barangay, current_home: $Acurrent_home, current_province: $Acurrent_province, current_sitio: $Acurrent_sitio, current_town: $Acurrent_town, date_of_birth: $Adate_of_birth, date_time_incident: $Adate_time_incident, date_time_reported: $Adate_time_reported, email: $Aemail, family_name: $Afamily_name, gender: $Agender, highest_education: $Ahighest_education, id_presented: $Aid_presented, last_name: $Alast_name, middle_name: $Amiddle_name, mobile: $Amobile, nickname: $Anickname, occupation: $Aoccupation, other_barangay: $Aother_barangay, other_home: $Aother_home, other_province: $Aother_province, other_sitio: $Aother_sitio, other_town: $Aother_town, phone: $Aphone, place_of_birth: $Aplace_of_birth, qualifier: $Aqualifier, status: $Astatus, type_of_incident: $Atype_of_incident, blotter_id: $Ablotter_id}) {
    affected_rows
  }
}
`
export const saveSuspect = gql`
mutation MyMutation($Bage: String, $Bcitizen: String, $Bcolor_hair: String, $Bcolor_of_eye: String, $Bcurrent_barangay: String, $Bcurrent_home: String, $Bcurrent_province: String, $Bcurrent_sitio: String, $Bcurrent_town: String, $Bdate_of_birth: String, $Bdesc_eye: String, $Bdesc_of_hair: String, $Bemail: String, $Bfamily_name: String, $Bgender: String, $Bgroup_affiliation: String, $Bheight: String, $Bhighest_education: String, $Blast_name: String, $Bmiddle_name: String, $Bmobile: String, $Bnickname: String, $Boccupation: String, $Bother_barangay: String, $Bother_home: String, $Bother_province: String, $Bother_sitio: String, $Bother_town: String, $Bpersonel_rank: String, $Bphone: String, $Bplace_of_birth: String, $Bprevious_criminal_record: String, $Bqualifier: String, $Brelation_to_victim: String, $Bstatus: String, $Bstatus_of_previous_case: String, $Bunder_influence_of: String, $Bunit_assignment: String, $Bweight: String, $Bwork_address: String, $Bblotter_id: uuid) {
  insert_ItemB(objects: {age: $Bage, citizen: $Bcitizen, color_hair: $Bcolor_hair, color_of_eye: $Bcolor_of_eye, current_barangay: $Bcurrent_barangay, current_home: $Bcurrent_home, current_province: $Bcurrent_province, current_sitio: $Bcurrent_sitio, current_town: $Bcurrent_town, date_of_birth: $Bdate_of_birth, desc_eye: $Bdesc_eye, desc_of_hair: $Bdesc_of_hair, email: $Bemail, family_name: $Bfamily_name, gender: $Bgender, group_affiliation: $Bgroup_affiliation, height: $Bheight, highest_education: $Bhighest_education, last_name: $Blast_name, middle_name: $Bmiddle_name, mobile: $Bmobile, nickname: $Bnickname, occupation: $Boccupation, other_barangay: $Bother_barangay, other_home: $Bother_home, other_province: $Bother_province, other_sitio: $Bother_sitio, other_town: $Bother_town, personel_rank: $Bpersonel_rank, phone: $Bphone, place_of_birth: $Bplace_of_birth, previous_criminal_record: $Bprevious_criminal_record, qualifier: $Bqualifier, relation_to_victim: $Brelation_to_victim, status: $Bstatus, status_of_previous_case: $Bstatus_of_previous_case, under_influence_of: $Bunder_influence_of, unit_assignment: $Bunit_assignment, weight: $Bweight, work_address: $Bwork_address, blotter_id: $Bblotter_id}) {
    affected_rows
  }
}
`
export const saveVictims = gql`
mutation MyMutation($Cage: String, $Cblotter_id: uuid, $Ccitizen: String, $Ccurrent_barangay: String, $Ccurrent_home: String, $Ccurrent_province: String, $Ccurrent_sitio: String, $Ccurrent_town: String, $Cdate_of_birth: String, $Cemail: String, $Cfamily_name: String, $Cgender: String, $Chighest_education: String, $Clast_name: String, $Cmiddle_name: String, $Cmobile: String, $Cnickname: String, $Coccupation: String, $Cother_barangay: String, $Cother_home: String, $Cother_province: String, $Cother_sitio: String, $Cother_town: String, $Cphone: String, $Cplace_of_birth: String, $Cqualifier: String, $Cstatus: String, $Cwork_address: String) {
  insert_ItemC(objects: {age: $Cage, blotter_id: $Cblotter_id, citizen: $Ccitizen, current_barangay: $Ccurrent_barangay, current_home: $Ccurrent_home, current_province: $Ccurrent_province, current_sitio: $Ccurrent_sitio, current_town: $Ccurrent_town, date_of_birth: $Cdate_of_birth, email: $Cemail, family_name: $Cfamily_name, gender: $Cgender, highest_education: $Chighest_education, last_name: $Clast_name, middle_name: $Cmiddle_name, mobile: $Cmobile, nickname: $Cnickname, occupation: $Coccupation, other_barangay: $Cother_barangay, other_home: $Cother_home, other_province: $Cother_province, other_sitio: $Cother_sitio, other_town: $Cother_town, phone: $Cphone, place_of_birth: $Cplace_of_birth, qualifier: $Cqualifier, status: $Cstatus, work_address: $Cwork_address}) {
    affected_rows
  }
}
`

export const saveNarrative = gql`
mutation MyMutation($Dblotter_entry_number: String, $Dblotter_id: uuid, $Dchief_instructions: String, $Ddate: String, $Ddesk_offiicer: String, $Dname_of_chief: String, $Dname_of_investigator: String, $Dnarrative_of_incident: String, $Dplace_of_incident: String, $Dreporting_person: String, $Dtime: String, $Dtype_of_incident: String) {
  insert_ItemD(objects: {blotter_entry_number: $Dblotter_entry_number, blotter_id: $Dblotter_id, chief_instructions: $Dchief_instructions, date: $Ddate, desk_offiicer: $Ddesk_offiicer, name_of_chief: $Dname_of_chief, name_of_investigator: $Dname_of_investigator, narrative_of_incident: $Dnarrative_of_incident, place_of_incident: $Dplace_of_incident, reporting_person: $Dreporting_person, time: $Dtime, type_of_incident: $Dtype_of_incident}) {
    affected_rows
  }
}
`
export const saveIncidentReciept = gql`
mutation MyMutation($IRblotter_id: uuid, $IRdate_incident: String, $IRdate_report: String, $IRdesk_officer: String, $IRname_of_reporting: String, $IRplace_incident: String, $IRtype_incident: String, $IRaddress_reporting: String, $ IRblotter_entry: String) {
  insert_Incident_record_reciept(objects: {blotter_id: $IRblotter_id, date_incident: $IRdate_incident, date_report: $IRdate_report, desk_officer: $IRdesk_officer, name_of_reporting: $IRname_of_reporting, place_incident: $IRplace_incident, type_incident: $IRtype_incident, address_reporting: $IRaddress_reporting, blotter_entry: $IRblotter_entry}) {
    affected_rows
  }
}
`
export const saveChildrenConflict = gql`
mutation MyMutation($CCblotter_id: uuid, $CCdeversion: String, $CCdistinguising: String, $CCguardian_address: String, $CCmobile: String, $CCname_of_guardian: String, $CCphone: String) {
  insert_children_in_conflict(objects: {blotter_id: $CCblotter_id, deversion: $CCdeversion, distinguising: $CCdistinguising, guardian_address: $CCguardian_address, mobile: $CCmobile, name_of_guardian: $CCname_of_guardian, phone: $CCphone}) {
    affected_rows
  }
}
`
export const saveIRTR = gql`
mutation MyMutation($station_name: String, $investigator: String, $chief: String, $chief_mobile: String, $station_telephone: String, $investigator_mobile: String, $blotter_id: uuid) {
  insert_IRTR(objects: {station_name: $station_name, investigator: $investigator, chief: $chief, chief_mobile: $chief_mobile, station_telephone: $station_telephone, blotter_id: $blotter_id, investigator_mobile: $investigator_mobile}) {
    affected_rows
  }
}
`
export const FillUpIncidentRecordForm = gql`
mutation MyMutation(
  $Aage: String,
  $Ablotter_entry_number: String,
  $Acitizen: String,
  $Acurrent_barangay: String,
  $Acurrent_home: String,
  $Acurrent_province: String,
  $Acurrent_sitio: String,
  $Acurrent_town: String,
  $Adate_of_birth: String,
  $Adate_time_incident: String,
  $Adate_time_reported: String,
  $Aemail: String,
  $Afamily_name: String,
  $Agender: String,
  $Ahighest_education: String,
  $Aid_presented: String,
  $Alast_name: String,
  $Amiddle_name: String,
  $Amobile: String,
  $Anickname: String,
  $Aoccupation: String,
  $Aother_barangay: String,
  $Aother_home: String,
  $Aother_province: String,
  $Aother_sitio: String,
  $Aother_town: String,
  $Aphone: String,
  $Aplace_of_birth: String,
  $Aqualifier: String,
  $Astatus: String,
  $Atype_of_incident: String,
  $Ablotter_id: uuid,


  $Bage: String,
  $Bcitizen: String,
  $Bcolor_hair:String,
  $Bcolor_of_eye: String,
  $Bcurrent_barangay: String,
  $Bcurrent_home: String,
  $Bcurrent_province: String,
  $Bcurrent_sitio: String,
  $Bcurrent_town: String,
  $Bdate_of_birth: String,
  $Bdesc_eye: String,
  $Bdesc_of_hair: String,
  $Bemail: String,
  $Bfamily_name: String,
  $Bgender: String,
  $Bgroup_affiliation: String,
  $Bheight: String,
  $Bhighest_education: String,
  $Blast_name: String,
  $Bmiddle_name: String,
  $Bmobile: String,
  $Bnickname: String,
  $Boccupation:  String,
  $Bother_barangay:  String,
  $Bother_home:  String,
  $Bother_province:  String,
  $Bother_sitio:  String,
  $Bother_town:  String,
  $Bpersonel_rank:  String,
  $Bphone:  String,
  $Bplace_of_birth:  String,
  $Bprevious_criminal_record: String,
  $Bqualifier:  String,
  $Brelation_to_victim:  String,
  $Bstatus:  String,
  $Bstatus_of_previous_case:  String,
  $Bunder_influence_of:  String,
  $Bunit_assignment:  String,
  $Bweight:  String,
  $Bwork_address:  String,
  $Bblotter_id: uuid,


  $Cage: String,
  $Cblotter_id: uuid,
  $Ccitizen: String,
  $Ccurrent_barangay: String,
  $Ccurrent_home: String,
  $Ccurrent_province: String,
  $Ccurrent_sitio: String,
  $Ccurrent_town: String,
  $Cdate_of_birth: String,
  $Cemail: String,
  $Cfamily_name: String,
  $Cgender: String,
  $Chighest_education: String,
  $Clast_name: String
  $Cmiddle_name: String,
  $Cmobile: String,
  $Cnickname: String,
  $Coccupation: String,
  $Cother_barangay: String,
  $Cother_home: String,
  $Cother_province: String,
  $Cother_sitio: String,
  $Cother_town: String,
  $Cphone: String,
  $Cplace_of_birth: String,
  $Cqualifier: String,
  $Cstatus: String,
  $Cwork_address: String,

  $Dblotter_entry_number: String,
  $Dblotter_id: uuid,
  $Dchief_instructions: String,
  $Ddate: String,
  $Ddesk_offiicer: String,
  $Dname_of_chief: String,
  $Dname_of_investigator: String,
  $Dnarrative_of_incident: String,
  $Dplace_of_incident: String,
  $Dreporting_person: String,
  $Dtime: String,
  $Dtype_of_incident: String,

  $IRblotter_id: uuid,
  $IRdate_incident: String,
  $IRdate_report: String,
  $IRdesk_officer: String,
  $IRname_of_reporting: String,
  $IRplace_incident: String,
  $IRtype_incident: String,
  $IRaddress_reporting: String,
  $IRblotter_entry: String,

  $CCblotter_id: uuid,
  $CCdeversion:  String,
  $CCdistinguising:  String,
  $CCguardian_address:  String,
  $CCmobile:  String,
  $CCname_of_guardian:  String,
  $CCphone: String
  ) {
  insert_blotter(objects:
    {ItemAs: {data: {
      age: $Aage,
      blotter_entry_number: $Ablotter_entry_number,
      citizen: $Acitizen,
      current_barangay: $Acurrent_barangay,
      current_home: $Acurrent_home,
      current_province: $Acurrent_province,
      current_sitio: $Acurrent_sitio,
      current_town: $Acurrent_town,
      date_of_birth: $Adate_of_birth,
      date_time_incident: $Adate_time_incident,
      date_time_reported: $Adate_time_reported,
      email: $Aemail,
      family_name: $Afamily_name,
      gender: $Agender,
      highest_education: $Ahighest_education,
      id_presented: $Aid_presented,
      last_name: $Alast_name,
      middle_name: $Amiddle_name,
      mobile: $Amobile,
      nickname: $Anickname,
      occupation: $Aoccupation,
      other_barangay: $Aother_barangay,
      other_home: $Aother_home,
      other_province: $Aother_province,
      other_sitio: $Aother_sitio,
      other_town: $Aother_town,
      phone: $Aphone,
      place_of_birth: $Aplace_of_birth,
      qualifier: $Aqualifier,
      status: $Astatus,
      type_of_incident: $Atype_of_incident,
      blotter_id: $Ablotter_id
    }},
    ItemBs: {data: {
      age: $Bage,
      citizen: $Bcitizen,
      color_hair: $Bcolor_hair,
      color_of_eye:  $Bcolor_of_eye,
      current_barangay: $Bcurrent_barangay,
      current_home: $Bcurrent_home,
      current_province: $Bcurrent_province,
      current_sitio: $Bcurrent_sitio,
      current_town: $Bcurrent_town,
      date_of_birth: $Bdate_of_birth,
      desc_eye: $Bdesc_eye,
      desc_of_hair: $Bdesc_of_hair,
      email: $Bemail,
      family_name: $Bfamily_name,
      gender: $Bgender,
      group_affiliation: $Bgroup_affiliation,
      height: $Bheight,
      highest_education: $Bhighest_education,
      last_name: $Blast_name,
      middle_name: $Bmiddle_name,
      mobile: $Bmobile,
      nickname: $Bnickname,
      occupation: $Boccupation,
      other_barangay: $Bother_barangay,
      other_home: $Bother_home,
      other_province: $Bother_province,
      other_sitio: $Bother_sitio,
      other_town: $Bother_town,
      personel_rank:  $Bpersonel_rank,
      phone: $Bphone,
      place_of_birth: $Bplace_of_birth,
      previous_criminal_record: $Bprevious_criminal_record,
      qualifier: $Bqualifier,
      relation_to_victim: $Brelation_to_victim,
      status: $Bstatus,
      status_of_previous_case: $Bstatus_of_previous_case,
      under_influence_of: $Bunder_influence_of,
      unit_assignment: $Bunit_assignment,
      weight: $Bweight,
      work_address: $Bwork_address,
      blotter_id: $Bblotter_id
    }},
    ItemCs: {data: {
      age: $Cage,
      blotter_id: $Cblotter_id,
      citizen: $Ccitizen,
      current_barangay: $Ccurrent_barangay ,
      current_home: $Ccurrent_home,
      current_province: $Ccurrent_province,
      current_sitio: $Ccurrent_sitio,
      current_town: $Ccurrent_town,
      date_of_birth: $Cdate_of_birth,
      email: $Cemail,
      family_name: $Cfamily_name,
      gender: $Cgender,
      highest_education: $Chighest_education,
      last_name: $Clast_name,
      middle_name: $Cmiddle_name ,
      mobile: $Cmobile,
      nickname: $Cnickname,
      occupation: $Coccupation,
      other_barangay: $Cother_barangay,
      other_home: $Cother_home,
      other_province: $Cother_province,
      other_sitio: $Cother_sitio,
      other_town: $Cother_town,
      phone: $Cphone,
      place_of_birth: $Cplace_of_birth,
      qualifier: $Cqualifier,
      status: $Cstatus,
      work_address: $Cwork_address
    }},
    ItemDs: {data: {
      blotter_entry_number: $Dblotter_entry_number,
      blotter_id: $Dblotter_id,
      chief_instructions: $Dchief_instructions,
      date: $Ddate,
      desk_offiicer: $Ddesk_offiicer,
      name_of_chief: $Dname_of_chief,
      name_of_investigator: $Dname_of_investigator,
      narrative_of_incident: $Dnarrative_of_incident,
      place_of_incident: $Dplace_of_incident,
      reporting_person: $Dreporting_person,
      time: $Dtime,
      type_of_incident: $Dtype_of_incident
    }},
    Incident_record_reciepts: {data: {
      blotter_id: $IRblotter_id,
      date_incident: $IRdate_incident,
      date_report: $IRdate_report,
      desk_officer: $IRdesk_officer,
      name_of_reporting: $IRname_of_reporting,
      place_incident: $IRplace_incident,
      type_incident: $IRtype_incident,
      address_reporting: $IRaddress_reporting
      blotter_entry: $IRblotter_entry
    }},
    children_in_conflicts: {data: {
      blotter_id: $CCblotter_id,
      deversion: $CCdeversion,
      distinguising: $CCdistinguising,
      guardian_address: $CCguardian_address,
      mobile: $CCmobile,
      name_of_guardian: $CCname_of_guardian,
      phone: $CCphone
    }}}) {

    affected_rows
  }
}
`
