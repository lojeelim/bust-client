import gql from 'graphql-tag'

export const ReportUser = gql`
mutation MyMutation($isReported: Int, $id: uuid) {
  update_users(where: {id: {_eq: $id}}, _set: {isReported: $isReported}) {
    affected_rows
  }
}
`
export const addUser = gql`mutation addPolice(
    $first_name: String,
    $last_name: String,
    $middle_name: String,
    $password: String,
    $username: String,
    $rank: String,
    $role: String
    $station_id: uuid,
    $dep_id: uuid,
    $birthday: String,
    $gender: String,
    $contact_number: String,
    $image: String,
    $user_type: String,
    $status: Boolean,
    $isNew: Boolean
  ) {
  insert_users
    (objects:
      {
        first_name: $first_name,
        last_name: $last_name,
        middle_name: $middle_name,
        password: $password,
        user_type: $user_type,
        username: $username,
        station_id: $station_id,
        rank: $rank,
        role: $role,
        birthday: $birthday
        dep_id: $dep_id
        gender: $gender
        contact_number: $contact_number
        image: $image
        status: $status
        isNew: $isNew
      }
    )
    {
      returning {
        id
        department {
          id
          dep_name
        }
      }
    }
  }
`
export const showPoliceUser = gql`query showPolice($station_id: uuid){
  users(where: {station_id: {_eq: $station_id}, _and: {user_type: {_eq: "police"}}}) {
    id
    first_name
    gender
    last_name
    role
    username
    middle_name
    image
    contact_number
    rank
    status
      department {
        id
        dep_name
      }
  }
}
`
export const showUserById = gql`query showPolice($id: uuid){
  users(where: {id: {_eq: $id}}) {
    id
    first_name
    last_name
    middle_name
    image
    contact_number
    user_type
    rank
    username
    status
  }
}
`
export const showBarangayUser = gql`query showPolice($station_id: uuid){
  users(where: {station_id: {_eq: $station_id}, _and: {user_type: {_eq: "barangay_member"}}}) {
    id
    status
    first_name
    gender
    last_name
    username
    middle_name
    image
    contact_number
    rank
  }
}
`
export const showAllUserByType = gql`query showPolice($station_id: uuid, $user_type: String){
  users(where: {station_id: {_eq: $station_id}, _and: {user_type: {_eq: $user_type}, _and: {status: {_eq: true}}}}) {
    id
    first_name
    gender
    last_name
    username
    middle_name
    image
    contact_number
    rank
  }
}
`
export const deleteUser = gql`mutation deletePolice($id: uuid!){
  delete_users_by_pk(id: $id){
      id
  }
}
`
export const updateUser = gql`mutation updatePolice(
  $id: uuid,
  $first_name: String,
  $last_name: String,
  $middle_name: String,
  $rank: String,
  $dep_id: uuid,
  $contact_number: String,
  $role: String,
  )
  {
    update_users(where:
      { id: { _eq: $id } },
      _set: {
        first_name: $first_name,
        last_name: $last_name,
        middle_name: $middle_name,
        rank: $rank,
        dep_id: $dep_id,
        contact_number: $contact_number,
        role: $role
      }
    )
  {
    returning {
      id
      department {
        id
        dep_name
      }
    }
  }
}
`
export const updateUserStatus = gql`mutation updatePolice(
  $id: uuid,
  $status: Boolean
  )
  {
    update_users(where:
      { id: { _eq: $id } },
      _set: {
        status: $status
      }
    )
  {
    returning {
      id
    }
  }
}
`
export const countUserNotification = gql`
query MyQuery($user_id: uuid) {
  users(where: {id: {_eq: $user_id}}) {
    id
    username
    first_name
    notifications_aggregate(where: {status: {_eq: "active"}}) {
      aggregate {
        count(columns: id)
      }
    }
  }
}
`
export const countRespondentNotification = gql`
query MyQuery($user_id: uuid) {
  users(where: {id: {_eq: $user_id}}) {
    id
    username
    first_name
    notificationsByRespondentId_aggregate(where: {status: {_eq: "active"}}) {
      aggregate {
        count(columns: id)
      }
    }
  }
}
`
// not in use query
export const showbuster = gql`
query MyQuery {
  users(where: {user_type: {_eq: "user"}}) {
    first_name
    gender
    id
    image
    last_name
    middle_name
    status
    username
    contact_number
    birthday
    user_type
    isReported
  }
}
`
export const showActiveBuster = gql`
query MyQuery {
  users(where: {user_type: {_eq: "user"}, _and: {_not: {isReported: {_gt: 0}}, _and: {isNew: {_eq: false}}}}) {
    first_name
    gender
    id
    image
    last_name
    middle_name
    status
    username
    contact_number
    birthday
    user_type
    isReported
  }
}
`
export const showReportedBuster = gql`
query MyQuery {
  users(where: {user_type: {_eq: "user"}, _and: {_and: {isReported: {_gt: 0}}}}) {
    first_name
    gender
    id
    image
    last_name
    middle_name
    status
    username
    contact_number
    birthday
    user_type
    isReported
  }
}
`
export const updateIsNew = gql`
mutation MyMutation($id: uuid) {
  update_users(where: {id: {_eq: $id}}, _set: {isNew: false}) {
    affected_rows
  }
}
`
export const showNewBuster = gql`
query MyQuery {
  users(where: {user_type: {_eq: "user"}, _and: {_not: {isReported: {_gt: 0}}, _and: {isNew: {_eq: true}}}}) {
    first_name
    gender
    id
    image
    last_name
    middle_name
    status
    username
    contact_number
    birthday
    user_type
    isReported
  }
}
`
