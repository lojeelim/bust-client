import gql from 'graphql-tag'

export const getIncident = gql`
query getincident($station_id: uuid) {
  incident(where: {station_id: {_eq: $station_id}}) {
    id
    incident_name
    incident_type
    incident_evid
    incident_desc
    longitude
    latitude
    created_at
    status_desc
    status
    user {
      first_name
      id
      image
      last_name
      middle_name
      username
      contact_number
    }
  }
}
`
export const UpdateStatus = gql`
mutation updateStatus($id: uuid, $status: Boolean) {
  update_incident(where: {id: {_eq: $id}}, _set: {status: $status}) {
    affected_rows
  }
}
`
export const UpdateStatusDesc = gql`
mutation updateStatus($id: uuid, $status_desc: String) {
  update_incident(where: {id: {_eq: $id}}, _set: {status_desc: $status_desc, status: true}) {
    affected_rows
  }
}
`
export const showIncident = gql`
query MyQuery {
  incident(where: {status: {_eq: true}}) {
    id
    incident_name
    incident_evid
    incident_desc
    incident_type
    created_at
    station_id
    updated_at
    user_id
    user {
      id
      username
      first_name
      middle_name
      last_name
      image
    }
  }
}
`
