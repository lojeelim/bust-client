import gql from 'graphql-tag'

export const saveNotification = gql`
mutation MyMutation($notification_content: String, $notification_type: String, $station_id: uuid, $user_id: uuid, $respondent_id: uuid, $complaint_id: uuid ){
    insert_notification(objects:
        {
            notification_content: $notification_content,
            notification_type:$notification_type,
            station_id: $station_id,
            user_id: $user_id
            complaint_id: $complaint_id
            respondent_id: $respondent_id
        }
    ) {
      returning {
        id
      }
    }
  }
`
export const getNotificationByUserId = gql`
query MyQuery($user_id: uuid) {
  notification(where: {user_id: {_eq: $user_id}}, order_by: {created_at: desc}) {
    id
    notification_content
    notification_type
    created_at
    userByRespondentId {
      id
      image
      gender
      first_name
      contact_number
      last_name
      middle_name
      rank
      status
      username
      user_type
    }
    station {
      latitude
      longitude
      station_logo
      station_name
      station_type
      id
    }
    complaint {
      complaint_statement
      complaint_type
      complaint_evid
      complaint_accuse_person
      id
      created_at
      station {
        station_name
        station_logo
        station_location
        station_type
        latitude
        isActive
        id
        longitude
      }
    }
  }
}
`
export const getNotificationByRespondentId = gql`
query MyQuery($user_id: uuid) {
  notification(where: {respondent_id: {_eq: $user_id}}, order_by: {created_at: desc}) {
    id
    notification_content
    notification_type
    created_at
    userByRespondentId {
      id
      image
      gender
      first_name
      contact_number
      last_name
      middle_name
      rank
      status
      username
      user_type
    }
    station {
      latitude
      longitude
      station_logo
      station_name
      station_type
      id
    }
    complaint {
      complaint_statement
      complaint_type
      complaint_evid
      complaint_accuse_person
      id
      created_at
      station {
        station_name
        station_logo
        station_location
        station_type
        latitude
        isActive
        id
        longitude
      }
    }
  }
}
`
export const getNotificationByStationIdasking = gql`
query MyQuery($station_id: uuid) {
  notification(where: {station_id: {_eq: $station_id}, _and: {notification_type: {_eq: "asking"}}}, order_by: {created_at: desc}) {
    id
    notification_content
    notification_type
    created_at
    userByRespondentId {
      id
      image
      gender
      first_name
      contact_number
      last_name
      middle_name
      rank
      status
      username
      user_type
    }
    station {
      latitude
      longitude
      station_logo
      station_name
      station_type
      id
    }
    complaint {
      user_id
      complaint_statement
      complaint_type
      complaint_evid
      complaint_accuse_person
      id
      created_at
      station {
        station_name
        station_logo
        station_location
        station_type
        latitude
        isActive
        id
        longitude
      }
      blotters {
        id
        ItemDs {
          id
          blotter_entry_number
          chief_instructions
          created_at
          date
          desk_offiicer
          name_of_chief
          name_of_investigator
          narrative_of_incident
          place_of_incident
          reporting_person
          time
          type_of_incident
        }
      }
    }
  }
}
`
export const getNotificationByStationId = gql`
query MyQuery($station_id: uuid) {
  notification(where: {station_id: {_eq: $station_id}, _not: {notification_type: {_eq: "asking"}}}, order_by: {created_at: desc}) {
    id
    notification_content
    notification_type
    created_at
    userByRespondentId {
      id
      image
      gender
      first_name
      contact_number
      last_name
      middle_name
      rank
      status
      username
      user_type
    }
    station {
      latitude
      longitude
      station_logo
      station_name
      station_type
      id
    }
    complaint {
      complaint_statement
      complaint_type
      complaint_evid
      complaint_accuse_person
      id
      created_at
      station {
        station_name
        station_logo
        station_location
        station_type
        latitude
        isActive
        id
        longitude
      }
    }
  }
}
`
export const UpdateUserNotificationStatus = gql`
mutation MyMutation($user_id: uuid) {
  update_notification(where: {user_id: {_eq: $user_id}}, _set: {status: "inactive"}) {
    affected_rows
  }
}
`

export const UpdateStationNotificationStatus = gql`
mutation MyMutation($station_id: uuid) {
  update_notification(where: {station_id: {_eq: $station_id}}, _set: {status: "inactive"}) {
    affected_rows
  }
}
`

export const UpdateRespondentNotificationStatus = gql`
mutation MyMutation($user_id: uuid) {
  update_notification(where: {respondent_id: {_eq: $user_id}}, _set: {status: "inactive"}) {
    affected_rows
  }
}
`
