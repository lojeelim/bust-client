import gql from 'graphql-tag'

export const showPoliceStation = gql`query showPoliceStation {
  station(where: {station_type: {_eq: "Police Station"}}) {
    id
    station_name
    station_type
    latitude
    longitude
    station_logo
    station_location
    isActive
    users_aggregate {
      aggregate {
        count(columns: id)
      }
    }
    users(where: {user_type: {_eq: "station_admin"}}) {
      first_name
      image
      last_name
      middle_name
      rank
      password
      status
      station_id
      username
      user_type
    }
  }
}
`
export const showBarangayHall = gql`query showPoliceStation {
  station(where: {station_type: {_eq: "Barangay Hall"}}) {
    id
    station_name
    station_type
    latitude
    longitude
    station_logo
    station_location
    isActive
    users_aggregate {
      aggregate {
        count(columns: id)
      }
    }
    users(where: {user_type: {_eq: "barangay_admin"}}) {
      first_name
      image
      last_name
      middle_name
      rank
      password
      status
      station_id
      username
      user_type
    }
  }
}
`
export const showAllStation = gql`
query showAllStation {
  station {
    id
    station_name
    station_type
    latitude
    longitude
    station_logo
    station_location
    isActive
  }
}
`
export const showAllStationByType = gql`
query showAllStationByType($station_type: String) {
  station(where: {station_type: {_eq: $station_type}, _and: {isActive: {_eq: true }}}) {
    id
    station_name
    station_type
    latitude
    longitude
    station_logo
    station_location
    isActive
  }
}
`
export const getStationById = gql`query MyQuery($station_id: uuid) {
  station(where: {id: {_eq: $station_id}}) {
    id
    station_name
    station_type
    latitude
    longitude
    station_logo
    station_location
    isActive
  }
}
`
export const createStation = gql`mutation createStation
(
  $station_type:String,
  $station_name: String,
  $station_location: String,
  $birthday: String,
  $gender: String
  $first_name: String,
  $middle_name: String,
  $last_name: String,
  $username: String,
  $password: String
  $rank: String
  $contact_number: String,
  $user_type: String,
  $image: String,
  $station_logo: String,
  $longitude: String,
  $latitude: String
)
{
  insert_station
  (objects:
    {users:
      {data:
        {
          first_name: $first_name,
          last_name: $last_name,
          middle_name: $middle_name,
          password: $password,
          username: $username
          rank: $rank
          birthday: $birthday
          gender: $gender
          contact_number: $contact_number
          user_type: $user_type
          image: $image
        }
      },
      station_type: $station_type,
      station_name: $station_name,
      station_location: $station_location,
      station_logo: $station_logo,
      longitude: $longitude,
      latitude: $latitude

    }
  )
  {
    returning
    {
      id
    }
  }
}
`
export const countStationNotification = gql`
query MyQuery($station_id: uuid) {
  station(where: {id: {_eq: $station_id}}) {
    id
    notifications_aggregate(where: {status: {_eq: "active"}}) {
      aggregate {
        count(columns: id)
      }
    }
  }
}
`
export const updateStationName = gql`
mutation MyMutation($id: uuid, $station_name: String) {
  update_station(where: {id: {_eq: $id}}, _set: {station_name: $station_name}) {
    affected_rows
  }
}
`
export const updateStationStatus = gql`mutation updatStatus(
  $id: uuid,
  $isActive: Boolean
  )
  {
    update_station(where:
      { id: { _eq: $id } },
      _set: {
        isActive: $isActive
      }
    )
  {
    returning {
      id
    }
  }
}
`
