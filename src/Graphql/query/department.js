import gql from 'graphql-tag'

export const showDepartment = gql`query show_department($station_id: uuid){
  department(where: {station_id: {_eq: $station_id}}) {
    id
    dep_name
    users_aggregate {
      aggregate {
        count(columns: id)
      }
    }
  }
}
`

export const showDepartmentById = gql`query show_departmentById($id: uuid){
  department(where: {id: {_eq: $id}}) {
    id
    dep_name
  }
}
`
export const updateDepartment = gql`mutation update_department($id: uuid, $dep_name: String) {
    update_department(where: { id: { _eq: $id } }, _set: { dep_name: $dep_name }) {
      affected_rows
  }
}
`
export const deleteDepartment = gql`mutation delete_department($id: uuid!) {
  delete_department_by_pk(id: $id)
    {
      id
  }
}
`
export const createDepartment = gql`mutation create_department($station_id: uuid, $dep_name: String) {
  insert_department
    (objects:
      {
        station_id: $station_id,
        dep_name: $dep_name
      }
    )
    {
      returning
      {
        id
      }
    }
  }
`
