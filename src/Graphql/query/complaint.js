import gql from 'graphql-tag'
export const getAppointment = gql`
query getComplaint($station_id: uuid) {
  complaint(where: {station_id: {_eq: $station_id}, _and: {to: {_is_null: false}, _and: {_not: {status: {_eq: "Filed"}}, _or: {_not: {status: {_eq: "Withdraw"}}}}}}) {
    id
    latitude
    longitude
    complaint_statement
    complaint_type
    complaint_evid
    complaint_accuse_person
    status
    created_at
    from
    to
    user {
      first_name
      id
      image
      last_name
      middle_name
      username
      contact_number
    }
    station {
      id
      station_logo
      station_name
      station_location
      station_type
    }
    respondent_activities {
      id
      investigation_statement
      longitude
      latitude
      user {
        id
        first_name
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
        image
      }
    }
  }
}
`
export const getAllComplaint = gql`
query getComplaint($station_id: uuid) {
  complaint(where: {station_id: {_eq: $station_id}, _and: {_not: {status: {_eq: "Filed"}}}}) {
    complaint_type
    id
  }
}
`
export const getComplaint = gql`
query getComplaint($station_id: uuid, $status: String) {
  complaint(where: {station_id: {_eq: $station_id}, _and: {status: {_eq: $status}, _not: {complaint_type: {_eq: "Citizen Arrest"}}}}) {
    id
    latitude
    longitude
    complaint_statement
    complaint_type
    complaint_evid
    complaint_accuse_person
    status
    created_at
    from
    to
    user {
      isReported
      first_name
      id
      image
      last_name
      middle_name
      username
      contact_number
    }
    station {
      id
      station_logo
      station_name
      station_location
      station_type
    }
    respondent_activities {
      id
      investigation_statement
      longitude
      latitude
      user {
        id
        first_name
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
        image
      }
    }
  }
}
`
export const getComplaintOnProcessAndinvestigated = gql`
query getComplaint($station_id: uuid) {
  complaint(where: {station_id: {_eq: $station_id}, _and: {_not: {status: {_eq: "Withdraw"}}, _and: {_not: {status: {_eq: "Pending"}}, _and: {_not: {status: {_eq: "For Appointment"}}, _and: {_not: {status: {_eq: "Confirmed"}}}}}}}) {
    id
    latitude
    longitude
    complaint_statement
    complaint_type
    complaint_evid
    complaint_accuse_person
    status
    created_at
    user {
      first_name
      id
      image
      last_name
      middle_name
      username
      contact_number
    }
    station {
      id
      station_logo
      station_name
      station_location
      station_type
    }
    respondent_activities {
      investigation_statement
      longitude
      latitude
      user {
        id
        first_name
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
        image
      }
    }
  }
}
`
export const submitComplaint = gql`
mutation saveComplaint(
  $complaint_type: String,
  $complaint_evid: String,
  $complaint_accuse_person: String,
  $complaint_statement: String,
  $station_id: uuid,
  $longitude: String,
  $latitude: String
  $user_id: uuid)
  {
    insert_complaint(objects: {
      complaint_statement: $complaint_statement
      complaint_type: $complaint_type,
      complaint_evid: $complaint_evid,
      complaint_accuse_person: $complaint_accuse_person,
      station_id: $station_id,
      longitude: $longitude,
      latitude: $latitude
      user_id: $user_id}
    ) {
      returning {
        id
      }
    }
}
`
// $complaint_type: String || complaint_type: $complaint_type
export const updateComplaint = gql`
mutation assign($id: uuid, $complaint_accuse_person: String, $complaint_statement: String ) {
  update_complaint(where: {id: {_eq: $id}}, _set: {complaint_accuse_person: $complaint_accuse_person, complaint_statement: $complaint_statement}) {
    affected_rows
  }
}
`
export const updateStatus = gql`
mutation updateStatus($id: uuid, $status: String) {
  update_complaint(where: {id: {_eq: $id}}, _set: {status: $status}) {
    affected_rows
  }
}
`
export const updateStation = gql`
mutation MyMutation($id: uuid, $station_id: uuid) {
  update_complaint(where: {id: {_eq: $id}}, _set: {station_id: $station_id}) {
    affected_rows
  }
}
`

export const getUserComplaint = gql`
query MyQuery($user_id: uuid) {
  complaint(where: {user_id: {_eq: $user_id}}, order_by: {created_at: desc}) {
    id
    complaint_accuse_person
    complaint_evid
    complaint_statement
    complaint_type
    status
    created_at
    from
    to
    station {
      id
      station_name
      station_location
      station_logo
    }
    respondent_activities {
      user {
        first_name
        id
        image
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
      }
    }
  }
}
`
export const deleteComplaint = gql`
mutation deleteComplaint($id: uuid!) {
  delete_complaint_by_pk(id: $id) {
    id
  }
}
`

export const submitCitizenArrest = gql`
mutation MyMutation($user_id: uuid, $station_id: uuid, $longitude: String, $latitude: String, $complaint_evid: String) {
  insert_complaint(objects: {user_id: $user_id, station_id: $station_id, latitude: $latitude, longitude: $longitude, complaint_evid: $complaint_evid, complaint_type: "Citizen Arrest"}) {
    affected_rows
  }
}
`

export const getStationCitizenArrest = gql`
query getComplaint($station_id: uuid, $status: String) {
  complaint(where: {station_id: {_eq: $station_id}, _and: {status: {_eq: $status}, _and: {complaint_type: {_eq: "Citizen Arrest"}}}}) {
    id
    latitude
    longitude
    complaint_statement
    complaint_type
    complaint_evid
    complaint_accuse_person
    status
    created_at
    user {
      first_name
      id
      image
      last_name
      middle_name
      username
      contact_number
    }
    station {
      id
      station_logo
      station_name
      station_location
      station_type
    }
    respondent_activities {
      id
      investigation_statement
      longitude
      latitude
      user {
        id
        first_name
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
        image
      }
    }
  }
}
`
export const setAppointment = gql`
mutation MyMutation($id: uuid, $from: date, $to: date) {
  update_complaint(where: {id: {_eq: $id}}, _set: {from: $from, to: $to}) {
    affected_rows
  }
}
`
export const getUserCitizenArrest = gql`
query MyQuery($user_id: uuid) {
  complaint(where: {user_id: {_eq: $user_id}, _and: {complaint_type: {_eq: "Citizen Arrest"}}}) {
    id
    complaint_accuse_person
    complaint_evid
    complaint_statement
    complaint_type
    status
    created_at
    station {
      id
      station_name
      station_location
      station_logo
    }
    respondent_activities {
      user {
        first_name
        id
        image
        last_name
        middle_name
        rank
        username
        user_type
        contact_number
      }
    }
  }
}
`
