import gql from 'graphql-tag'

export const saveActivity = gql`
mutation MyMutation($user_id: uuid, $complaint_id: uuid, $incident_id: uuid, $station_id: uuid) {
  insert_respondent_activity(objects: {user_id: $user_id, complaint_id: $complaint_id, incident_id: $incident_id, station_id: $station_id}) {
    returning {
      id
      user {
        username
      }
    }
  }
}
`
export const showActivities = gql`query MyQuery($user_id: uuid) {
  respondent_activity(where: {user_id: {_eq: $user_id}}) {
    id
    investigation_statement
    longitude
    latitude
    user {
      id
      first_name
      last_name
      middle_name
      image
      rank
      status
      username
      user_type
    }
    complaint {
      complaint_statement
      complaint_evid
      complaint_accuse_person
      complaint_type
      created_at
      id
      longitude
      latitude
      station_id
      status
      user {
        contact_number
        first_name
        last_name
        middle_name
        image
        id
        username
        user_type
      }
    }
    incident {
      incident_name
      latitude
      longitude
      id
      created_at
      status
      user {
        image
        id
        first_name
        contact_number
        username
        last_name
        middle_name
      }
    }
  }
}
`
export const updateInvestigationStatement = gql`
mutation MyMutation($id: uuid, $longitude: String, $latitude: String, $investigation_statement: String) {
  update_respondent_activity(where: {id: {_eq: $id}}, _set: {longitude: $longitude, latitude: $latitude, investigation_statement: $investigation_statement}) {
    affected_rows
  }
}
`
