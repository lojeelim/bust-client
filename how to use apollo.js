// async created () { // async the function for query or mutation

  const { data } = await this.$apollo.query({
    query: this.$gql`query MyQuery {
        users {
          first_name
          id
          last_name
        }
      }
    `
  })


  let variables = {
    first_name: "allan",
    last_name: "layese",
    stattion_id: "c01156fb-7eb7-43af-89ac-d2daf694177a",
    password: "anypassword",
    username: "test"
  }

  const { data } = await this.$apollo.mutate({
    mutation: this.$gql`mutation MyMutation (
        $first_name: String,
        $last_name: String,
        $middle_name: String
      ) {
      insert_users(objects: {
        first_name: $first_name,
        last_name: $last_name,
        middle_name: $middle_name,
        password: $password, 
        station_id: $station_id, 
        username: $username}) 
        {
          returning {
            id
        }
      }
    }
    `, 
    variables
  })
